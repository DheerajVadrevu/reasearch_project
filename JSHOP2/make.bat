@echo off

if (%1) == (c) goto :Compile
if (%1) == (d) goto :Doc

if (%1) == (2) goto :Run2
if (%1) == (3) goto :Run3
if (%1) == (4) goto :Run4

goto :Compile

:Compile
  cd src\JSHOP2
  java antlr.Tool JSHOP2.g
  javac *.java
  cd ..
  jar cvf JSHOP2.jar JSHOP2\*.class
  del JSHOP2\*.class
  del JSHOP2\JSHOP2Lexer.*
  del JSHOP2\JSHOP2Parser.*
  del JSHOP2\JSHOP2TokenTypes.java
  del JSHOP2\JSHOP2TokenTypes.txt
  move JSHOP2.jar ..\bin
  cd ..
  goto :End

:Doc
  del doc/s/q > NUL
  cd src
  javadoc -d ..\doc -author -version -private JSHOP2
  cd ..
  goto :End



:Run2
  cd examples\officeroom
  java JSHOP2.InternalDomain basic_officeroom
  java JSHOP2.InternalDomain -ra problem_officeroom
  javac problem_officeroom.java
  java problem_officeroom
  del basic_officeroom.java
  del basic_officeroom.txt
  del problem_officeroom.java
  del *.class
  cd ..\..
  goto :End

:Run3
  cd examples\meetingroom
  java JSHOP2.InternalDomain basic_meetingroom
  java JSHOP2.InternalDomain -ra problem_meetingroom
  javac problem_meetingroom.java
  java problem_meetingroom
  del basic_meetingroom.java
  del basic_meetingroom.txt
  del problem_meetingroom.java
  del *.class
  cd ..\..
  goto :End

:Run4
  cd examples\breakoutarea
  java JSHOP2.InternalDomain basic_breakoutarea
  java JSHOP2.InternalDomain -ra problem_breakoutarea
  javac problem_breakoutarea.java
  java problem_breakoutarea
  del basic_breakoutarea.java
  del basic_breakoutarea.txt
  del problem_breakoutarea.java
  del *.class
  cd ..\..
  goto :End



:End
