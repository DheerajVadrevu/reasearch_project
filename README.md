This project is titled "Human Activity Recognition for Buildings Using AI Planning".

The project is inclusive of installation of the JSHOP2 planner. The guide to install and run the planner is in the 'README' file of the JSHOP2 planner.

After making sure the planner is properly installed, the execution of the project may begin.

All the scripts used outside of the planner are written in python, so make sure you have python installed properly on your machine.
________________________________________________________________________________________________________________________________________
In the folder 'Python_pubsubrp':
________________________________________________________________________________________________________________________________________

-Building_Simulator.py:  Runs the simulators of the office room, meeting room, breakout area in parallel for the activity recognition.

-Displaydata_table.py: This script initializes and clears the final output text files.

-filepaths.py: The paths of the necessary files to be modified/executed and some global varaiables are stored here. **These paths point to the location of the files on the machine where the project is executed and may be changed when the project is run on a different machine.

-Finaloutput_breakout.txt: Displays the final output of activity recognition in the breakout area. (All ouputs were last updated on 11th May, 2022).

-Finaloutput_meeting.txt: Displays the final output of activity recognition in the meeting room.(All ouputs were last updated on 11th May, 2022).

-Finaloutput_office.txt: Displays the final output of activity recognition in the office room.(All ouputs were last updated on 11th May, 2022).

-MakePlansReadable.py: This script takes the initial output from the planner and writes it to "Readableplan" files. Then this output is written to the "Finaloutput" files in a reader-friendly manner.

-MotiondetectionSensor.py: Simulated motion detection sensor.

-PlugwisemicrowaveSensor.py: Simulated Plugwise sensor of the microwave oven.

-PlugwisepcSensor.py:  Simulated Plugwise sensor of the PC.

-Plugwiseprojectorsensor.py: Simulated Plugwise sensor of the projector.

-PlugwisescreenSensor.py: Simulated Plugwise sensor of the PC screen.

-PressureSensor.py: Simulated Pressure sensor.

-VoicedetectionSensor.py: Simulated voice detection sensor.

-plan_breakoutarea.txt: Direct output from the planner for activity recognition in the breakout area.

-plan_meetingroom.txt: Direct output from the planner for activity recognition in the meeting room.

-plan_officeroom.txt: Direct output from the planner for activity recognition in the office room.

-Planner scripts for all the rooms(.py): Take in the outputs from simulated sensors of the corresponding rooms and the run the planner.(Activities other than absence are detected only after 3 successive iterations where the required conditions are true for 3 successive iterations to avoid sudden variations).

-Readableplan files(.txt): Files written as secondary outputs of the planner, used to write the outputs to the "Finaloutput" files in a reader-friendly manner.

-Sensor files(.txt): Files containing only the sensor outputs from the respective rooms.

-SensorThresholds.py: Contains the user defined thresholds of all the sensors.

-Simulator scripts for all the rooms(.py): Invoke the planner scripts and simulate activity recognition in the respective rooms.
















