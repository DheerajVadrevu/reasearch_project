import sys
import filepaths
def writereableplanfileOffice():
    filename = filepaths.path_readableplanfile_officeroom
    filename1= filepaths.path_planoutputfile_officeroom
    with open(filename1, "r") as f:
                with open(filename,"w") as f1:
                     for line in f:
                         if "!" in line:
                            f1.write(line.replace("!","").replace("(","").replace(")",""))
                            if  "ypc" in line:
                                f1.write(line.replace("!","").replace("ypc","Working with a pc").replace("(","").replace(")",""))
                            elif "npc" in line:
                                f1.write(line.replace("!","").replace("npc","Working without a pc").replace("(","").replace(")",""))
                            elif "presence" in line:
                                f1.write(line.replace("!","").replace("presence","Presence with no recognisable activity").replace("(","").replace(")",""))
                                
                         elif "0 plan(s)" in line:
                                 f1.write(line.replace("0 plan(s) were found:","This activity cannot be recognised"))
                             



def clearReadableplanfileOffice():
    filep=open(filepaths.path_readableplanfile_officeroom,"w")
    filep.truncate()

def writereableplanfileMeeting():
    filename = filepaths.path_readableplanfile_meetingroom
    filename1= filepaths.path_planoutputfile_meetingroom
    with open(filename1, "r") as f:
                with open(filename,"w") as f1:
                     for line in f:
                         if "!" in line:
                            f1.write(line.replace("!","").replace("(","").replace(")",""))
                            if "hm" in line:
                                f1.write(line.replace("!","").replace("hm","Meeting in progress").replace("(","").replace(")",""))
                            elif "presence" in line:
                                f1.write(line.replace("!","").replace("presence","Presence with no recognisable activity").replace("(","").replace(")",""))
                         elif "0 plan(s)" in line:
                                 f1.write(line.replace("0 plan(s) were found:","This activity cannot be recognised"))
                            
                             


def clearReadableplanfileMeeting():
    filep=open(filepaths.path_readableplanfile_meetingroom,"w")
    filep.truncate()


def writereableplanfileBreakout():
    filename = filepaths.path_readableplanfile_breakoutarea
    filename1= filepaths.path_planoutputfile_breakoutarea
    with open(filename1, "r") as f:
                with open(filename,"w") as f1:
                     for line in f:
                         if "!" in line:
                            f1.write(line.replace("!","").replace("(","").replace(")",""))
                            if "hcb" in line:
                                f1.write(line.replace("!","").replace("hcb","Coffee Break in progress").replace("(","").replace(")",""))
                            elif "presence" in line:
                                f1.write(line.replace("!","").replace("presence","Presence with no recognisable activity").replace("(","").replace(")",""))
                         elif "0 plan(s)" in line:
                                 f1.write(line.replace("0 plan(s) were found:","This activity cannot be recognised"))

def clearReadableplanfileBreakout():
    filep=open(filepaths.path_readableplanfile_breakoutarea,"w")
    filep.truncate()



writereableplanfileOffice()
writereableplanfileMeeting()
writereableplanfileBreakout()


