from datetime import datetime
from distutils.command.clean import clean
from os import truncate
import time
import string
import filepaths


import MakePlansReadable
import Displaydata_table
from tabulate import tabulate
import pandas as pd


import Planner_office_room
import sys
import logging

dataset4={}
class Simulator:
    def __init__(self,interval):
        self.interval=interval
    def start(self):
       
        
        
        
        i=1
        Planner_office_room.clear_input_sensordata_file()
        Planner_office_room.clear_output_plan_file()
        MakePlansReadable.clearReadableplanfileOffice()
        Displaydata_table.clearFinaloutputfile_office()
        Displaydata_table.initializeFinaloutputfile_office()
        Planner_office_room.counter_wp=0
        Planner_office_room.counter_wop=0
        while i<=10:
            sys.stdout=open(filepaths.path_sensoroutputsfile_officeroom,"a")
            
            
            
            time.sleep(self.interval)
            
            
            Planner_office_room.writeProblemFile()
            Planner_office_room.runActivity_recognition_example()
            MakePlansReadable.writereableplanfileOffice()
            file1=filepaths.path_readableplanfile_officeroom
   
            with open(file1, "r") as f:
               for line in f:
                    global dataset4
                    dataset4={"Activity_detected":line}
                            
            datasetfinal1=dict(**Planner_office_room.dataset1,**dataset4)

            df = pd.DataFrame(datasetfinal1)
            sys.stdout=open(filepaths.path_finaloutputfile_officeroom,"a")
            print(tabulate(df.T))
            
            i+=1
            
             
          
s = Simulator(1)
s.start()




