from random import random
class Plugwiseprojector:
    sensorType = "plugwiseprojector"
    instanceID = "32kd403kwpr"
    unit = "kWh"
    def __init__(self, min, max,avg):
        self. min =  min
        self.max = max
        self.avg=avg
        
        self.value = 0.0
    def sense(self):
    
        self.value = self.complexRandom()
        return self.value
   
    def complexRandom(self):
        value = self.avg * (3 * random() - 1)
        value = max(value, self.min)
        value = min(value, self.max)
        return value 

wpr= Plugwiseprojector(0.001,0.252,0.1265)
print(wpr.sense())