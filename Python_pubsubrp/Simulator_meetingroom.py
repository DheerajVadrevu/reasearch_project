from datetime import datetime
from distutils.command.clean import clean
from os import truncate
import time
import MakePlansReadable
from tabulate import tabulate
import pandas as pd
import Displaydata_table
import filepaths


import Planner_meetingroom
import sys
import logging
dataset5={}
class Simulator:
    def __init__(self,interval):
        self.interval=interval
    def start(self):
        
        i=1
        Planner_meetingroom.clear_input_sensordata_file()
        Planner_meetingroom.clear_output_plan_file()
        MakePlansReadable.clearReadableplanfileMeeting()
        Displaydata_table.clearFinaloutputfile_meeting()
        Displaydata_table.initializeFinaloutputfile_meeting()
        Planner_meetingroom.counter_hm=0
        
        
        while i<=10:
            sys.stdout=open(filepaths.path_sensoroutputsfile_meetingroom,"a")
            
            time.sleep(self.interval)
           
            Planner_meetingroom.writeProblemFile()
            
            Planner_meetingroom.runActivity_recognition_example()
            MakePlansReadable.writereableplanfileMeeting()

            file2=filepaths.path_readableplanfile_meetingroom
   
            with open(file2, "r") as f:
               for line in f:
                    global dataset5
                    dataset5={"Activity_detected":line}
                            
            datasetfinal2=dict(**Planner_meetingroom.datasetres,**dataset5)
            df = pd.DataFrame(datasetfinal2)
            
            sys.stdout=open(filepaths.path_finaloutputfile_meetingroom,"a")
            print(tabulate(df.T))
           
            i+=1
            
            
            
          
s = Simulator(1)
s.start()




