import multiprocessing as mp
from multiprocessing.dummy import freeze_support
import os
import time
from threading import Thread
import filepaths


def start_office_simulator():
    os.system("{0}{1}{2}".format(filepaths.universalpythonstatement,filepaths.universalwhitespace,filepaths.path_pythonsimulator_officeroom))

def start_meetingroom_simulator():
    os.system("{0}{1}{2}".format(filepaths.universalpythonstatement,filepaths.universalwhitespace,filepaths.path_pythonsimulator_meetingroom))

def start_breakoutarea_simulator():
    os.system("{0}{1}{2}".format(filepaths.universalpythonstatement,filepaths.universalwhitespace,filepaths.path_pythonsimulator_breakoutarea))

 
p1=mp.Process(target=start_office_simulator)   
p2=mp.Process(target=start_meetingroom_simulator)
p3=mp.Process(target=start_breakoutarea_simulator) 


if __name__=="__main__":
    p1.start()
    p2.start()
    p3.start()
    p1.join() 
    p2.join()
    p3.join()

