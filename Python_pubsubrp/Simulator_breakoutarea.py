from datetime import datetime
from distutils.command.clean import clean
from os import truncate
import time
import MakePlansReadable
from tabulate import tabulate
import pandas as pd
import Displaydata_table
import filepaths

import Planner_breakoutarea
import sys
import logging
dataset6={}
class Simulator:
    def __init__(self,interval):
        self.interval=interval
    def start(self):
       
       
       
        i=1
        Planner_breakoutarea.clear_input_sensordata_file()
        Planner_breakoutarea.clear_output_plan_file()
        MakePlansReadable.clearReadableplanfileBreakout()
        Displaydata_table.clearFinaloutputfile_breakout()
        Displaydata_table.initializeFinaloutputfile_breakout()
        Planner_breakoutarea.counter_hc=0
        
        while i<=10:
            sys.stdout=open(filepaths.path_sensoroutputsfile_breakoutarea,"a")
            
        
            
            time.sleep(self.interval)
            
            Planner_breakoutarea.writeProblemFile()
           
            Planner_breakoutarea.runActivity_recognition_example()
            MakePlansReadable.writereableplanfileBreakout()
            file3=filepaths.path_readableplanfile_breakoutarea
   
            with open(file3, "r") as f:
               for line in f:
                    global dataset6
                    dataset6={"Activity_detected":line}
                            
            datasetfinal3=dict(**Planner_breakoutarea.dataset3,**dataset6)
            df = pd.DataFrame(datasetfinal3)
            sys.stdout=open(filepaths.path_finaloutputfile_breakoutarea,"a")
            print(tabulate(df.T))
            i+=1
            
            
          
s = Simulator(1)
s.start()




