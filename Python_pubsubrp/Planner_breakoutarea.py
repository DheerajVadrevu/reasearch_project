import os
from turtle import clear
import SensorThresholds
from numpy import delete
from datetime import datetime
import MotiondetectionSensor
import PlugwisemicrowaveSensor
from tabulate import tabulate
import pandas as pd
import filepaths
import sys
counter_hc=0


dataset3={}
plugwisemicrowaves=[]

def writeProblemFile():
   
    global plugwisemicrowaves
    instances=5
    i=0
    plugwisemicrowaves=[]
    while(i<=instances):
        plugwisemicrowaves.append(PlugwisemicrowaveSensor.wmw.sense())
        i=i+1
    
    motion=MotiondetectionSensor.ms.sense()
    global dataset3
    dataset3={"Plugwisemicrowave_sensors_IDs_3mps12345":[plugwisemicrowaves],"Motion_sensor_ID_3ms1":[motion],"time":[datetime.now()]}
    df = pd.DataFrame(dataset3)
    print(tabulate(df.T))
    global counter_hc
    output= """(defproblem problem_breakoutarea basic_breakoutarea
  
    
            ((area breakout_area)"""


    if (all(i >=SensorThresholds.PlugwisemicrowaveThreshold for i in plugwisemicrowaves)):
        microwave_on="""(microwave_on microwaves)"""
        condition1=True
    else:
        microwave_off="""(microwave_off microwaves)"""
        condition1=False

    if (motion==SensorThresholds.MotionThreshold):
        motion_true="""(motion_true motion_found)"""
        condition2=True
    else:
        motion_false="""(motion_false motion_found)"""
        condition2=False

    
   

    if (condition1 and condition2):
        


    
      
      counter_hc=counter_hc+1
      
        
      if(counter_hc>=3):
         
           
        output+="\n"+ microwave_on+"\n"+motion_true+"\n"+"""
                        
                        
                        )  
                            
                        (
                        (recognise_activity)
                        ) 
                        
                        )
                                                """
      else:
        
       
        
        output+="\n"+ """
                            )  
                                
                            (
                            (recognise_activity)
                            ) 
                            
                            )
                            """



    




                
    else:


        counter_hc=0
        output+="\n"+ """
                            
                            )  
                                
                            (
                            (recognise_activity)
                            ) 
                            
                            )
                            """

    filename = filepaths.path_problemfile_breakoutarea
    with open(filename, "w") as f:
                f.truncate()
                f.write(output)
    


def clear_input_sensordata_file():
    input_file=open(filepaths.path_sensoroutputsfile_breakoutarea,"w")
    input_file.truncate()

    
def clear_output_plan_file():
    output_file=open(filepaths.path_planoutputfile_breakoutarea,"w")
    output_file.truncate()


def runActivity_recognition_example():
    os.chdir(filepaths.path_jshop2_directory)
    os.system("{0}{1}{2}{3}{4}{5}{6}".format(filepaths.universalmakestatement,filepaths.universalwhitespace,filepaths.makenumberforbreakoutarea,filepaths.universalwhitespace,filepaths.universalforwardsymbol,filepaths.universalwhitespace,filepaths.path_planoutputfile_breakoutarea))
    
    

    

