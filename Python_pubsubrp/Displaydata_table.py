from tabulate import tabulate
import pandas as pd
import filepaths

import sys
def clearFinaloutputfile_office():
    file1=open(filepaths.path_finaloutputfile_officeroom,"w")
    file1.truncate()

def clearFinaloutputfile_meeting():
    file1=open(filepaths.path_finaloutputfile_meetingroom,"w")
    file1.truncate()

def clearFinaloutputfile_breakout():
    file1=open(filepaths.path_finaloutputfile_breakoutarea,"w")
    file1.truncate()

def initializeFinaloutputfile_office():
    sys.stdout=open(filepaths.path_finaloutputfile_officeroom,"a")
    print("*******************************************************************Office Room**********************************************************************************************************************************************")

def initializeFinaloutputfile_meeting():
    sys.stdout=open(filepaths.path_finaloutputfile_meetingroom,"a")
    print("*******************************************************************Meeting Room*********************************************************************************************************************************************")

def initializeFinaloutputfile_breakout():
    sys.stdout=open(filepaths.path_finaloutputfile_breakoutarea,"a")
    print("*******************************************************************Breakout Area********************************************************************************************************************************************")