from random import random
class SeatPressureSensor:
    sensorType = "pressure"
    instanceID = "32kd403kp"
    unit = "newtons"
    def __init__(self, minPressure, maxPressure,averagePressure):
        self. minPressure =  minPressure
        self.maxPressure = maxPressure
        self.averagePressure = averagePressure
        #self.pressureVariation=pressureVariation
        self.value = 0.0
    def sense(self):
    
        self.value = self.complexRandom()
        return self.value
   
    def complexRandom(self):
        value = self.averagePressure * (3 * random() - 1)
        value = max(value, self.minPressure)
        value = min(value, self.maxPressure)
        return value 

ts= SeatPressureSensor(10,100,20)
print(ts.sense())


