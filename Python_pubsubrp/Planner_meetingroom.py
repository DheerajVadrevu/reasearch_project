import os
import PressureSensor
import VoicedetectionSensor
import Plugwiseprojectorsensor
import PressureSensor
import MotiondetectionSensor
import sys
import SensorThresholds
from tabulate import tabulate
import pandas as pd
from datetime import datetime
import filepaths
counter_hm=0
counter_p1=0

dataset2={}
dataset3={}
datasetres={}
pressure=[]

def writeProblemFile():
    global pressure
    instances=5
    i=0
    pressure=[]
    while(i<=instances):
        pressure.append(PressureSensor.ts.sense())
        i=i+1
    plugwiseprojector=Plugwiseprojectorsensor.wpr.sense()
    voice=VoicedetectionSensor.vs.sense()
    motion=MotiondetectionSensor.ms.sense()
    
    global dataset2
    dataset2={"Pressure_sensors_IDs_2ps12345":[pressure]}
    global dataset3
    dataset3={"Plugwiseprojector_sensor_ID_2pp1":[plugwiseprojector],"Voice_sensor_ID_2vs1":[voice],"Motion_sensor_ID_2ms1":[motion],"time":[datetime.now()]}
    global datasetres
    datasetres=dict(**dataset2,**dataset3)
    
    df=pd.DataFrame(datasetres)
    print(tabulate(df.T))
    global counter_hm
    global counter_p1
    output= """(defproblem problem_meetingroom basic_meetingroom
  
    
            ((area meeting_room)"""


    if (all(i >= SensorThresholds.PressureThreshold for i in pressure)):
        pressure_true="""(pressure_true chairs)"""
        condition1=True
    else:
        pressure_false="""(pressure_false chairs)"""
        condition1=False

    if (plugwiseprojector>=SensorThresholds.PlugwiseprojectorThreshold):
        projector_on="""(projector_on projector)"""
        condition2=True
    else:
        projector_off="""(projector_off projector)"""
        condition2=False

    if (voice==SensorThresholds.VoiceThreshold):
        voice_true="""(voice_true voice_heard)"""
        condition3=True
    else:
        voice_false="""(voice_false voice_heard)"""
        condition3=False

    if (motion==SensorThresholds.MotionThreshold):
        motion_true="""(motion_true motion_found)"""
        condition4=True
    else:
        motion_false="""(motion_false motion_found)"""
        condition4=False
   



    if( condition1 and condition2 and condition3):
      
      counter_hm=counter_hm+1
      counter_p1=0
        
      if(counter_hm>=3): 
          
        output+="\n"+ pressure_true+"\n"+projector_on+"\n"+voice_true+"\n"+"""

                            
                            )  
                                
                            (
                            (recognise_activity)
                            ) 
                            
                            )

                        """
      else:
        
        
        output+="\n"+ """
                            
                            )  
                                
                            (
                            (recognise_activity)
                            ) 
                            
                            )
                            """         



    elif(condition4 and not(condition1) and not(condition2) and not(condition3)):
            
        counter_p1=counter_p1+1
        counter_hm=0
        if(counter_p1>=3):     
             output+="\n"+motion_true+"\n"+projector_off+"\n"+pressure_false+"\n"+voice_false+"\n"+""")
                            
                              
                                
                            (
                            (recognise_activity )
                            ) 
                            
                            )
                            """
        
        else:
           
      
       
              
            output+="\n"+ """
                        )  
                            
                        (
                        (recognise_activity)
                        ) 
                        
                        )"""



    else:

        counter_hm=0 
        counter_p1=0 
        output+= "\n"+"""
                            )  
                                
                            (
                            (recognise_activity)
                            ) 
                            
                            )
                            """

    filename = filepaths.path_problemfile_meetingroom
    with open(filename, "w") as f:
                f.truncate()
                f.write(output)
   


def clear_input_sensordata_file():
    input_file=open(filepaths.path_sensoroutputsfile_meetingroom,"w")
    input_file.truncate()

    
def clear_output_plan_file():
    output_file=open(filepaths.path_planoutputfile_meetingroom,"w")
    output_file.truncate()


def runActivity_recognition_example():
    os.chdir(filepaths.path_jshop2_directory)
    os.system("{0}{1}{2}{3}{4}{5}{6}".format(filepaths.universalmakestatement,filepaths.universalwhitespace,filepaths.makenumberformeetingroom,filepaths.universalwhitespace,filepaths.universalforwardsymbol,filepaths.universalwhitespace,filepaths.path_planoutputfile_meetingroom))
    

    