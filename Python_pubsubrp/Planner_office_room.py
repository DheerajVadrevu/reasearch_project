import os
import time
from datetime import datetime
import sys
import PressureSensor
import PlugwisescreenSensor
import PlugwisepcSensor
import MotiondetectionSensor
from tabulate import tabulate
import pandas as pd
import SensorThresholds
import filepaths
counter_wp=0
counter_wop=0
counter_p=0


dataset1={}






def writeProblemFile():
    
    pressure=PressureSensor.ts.sense()
    plugwisescreen=PlugwisescreenSensor.wm.sense()
    plugwisepc=PlugwisepcSensor.ws.sense()
    motion=MotiondetectionSensor.ms.sense()
    Employee_ID="E01";
    global dataset1
    dataset1={"Employee_ID":[Employee_ID],"Pressure_sensor_ID_1ps1":[pressure],"Plugwisepc_sensor_ID_1pc1":[plugwisescreen],"Plugwisescreen_sensor_ID_1ss1":[plugwisepc],"Motion_sensor_ID_1ms1":[motion],"time":[datetime.now()]}
    df = pd.DataFrame(dataset1)
    print(tabulate(df.T))
    global counter_wp
    global counter_wop
    global counter_p


    output= """(defproblem problem_officeroom basic_officeroom
  
    
            ( (area office_room)
               (employee_id E01)"""


    if (pressure>=SensorThresholds.PressureThreshold):
        pressure_true="""(pressure_true chair)"""
        condition1=True
    else:
        pressure_false="""(pressure_false chair)"""
        condition1=False

    if (plugwisepc>=SensorThresholds.PlugwisepcThreshold):
        pc_on="""(pc_on computer)"""
        condition2=True
    else:
        pc_off="""(pc_off computer)"""
        condition2=False

    if (plugwisescreen>=SensorThresholds.PlugwisescreenThreshold):
        screen_on="""(screen_on screen)"""
        condition3=True
    else:
        screen_off="""(screen_off screen)"""
        condition3=False

    
    if (motion==SensorThresholds.MotionThreshold):
        motion_true="""(motion_true motion_found)"""
        condition4=True
    else:
        motion_false="""(motion_false motion_found)"""
        condition4=False
 

    
    if (condition1 and condition2 and condition3):

       


        counter_wop=0
        counter_p=0
        counter_wp=counter_wp+1
        
        if(counter_wp>=3):
            
               output+="\n"+pc_on+"\n"+pressure_true+"\n"+screen_on+"\n"+""")  
               
    
                        (
                        (recognise_activity)
                        ) 
                        
                        )"""

        else:
           
      
       
              
            output+="\n"+ """
                        )  
                            
                        (
                        (recognise_activity)
                        ) 
                        
                        )"""
                        
    elif(condition4 and not(condition1) and not(condition2) and not(condition3)):
        counter_wop=0
        counter_wp=0   
        counter_p=counter_p+1

        if(counter_p>=3):     
             output+="\n"+motion_true+"\n"+pc_off+"\n"+pressure_false+"\n"+screen_off+"\n"+""")
                            
                              
                                
                            (
                            (recognise_activity )
                            ) 
                            
                            )
                            """
        
        else:
           
      
       
              
            output+="\n"+ """
                        )  
                            
                        (
                        (recognise_activity)
                        ) 
                        
                        )"""                    

    elif(condition1 and not(condition2) and not(condition3)):
            
        counter_wop=counter_wop+1
        counter_p=0
        counter_wp=0
        if(counter_wop>=3):     
             output+="\n"+pressure_true+"\n"+pc_off+"\n"+screen_off+"\n"+ """)
                            
                              
                                
                            (
                            (recognise_activity )
                            ) 
                            
                            )
                            """
    
        else:
           
      
       
              
            output+="\n"+ """
                        )  
                            
                        (
                        (recognise_activity)
                        ) 
                        
                        )"""


    else:
           
      
        counter_wp=0
        counter_wop=0
        counter_p=0
              
        output+="\n"+ """
                                    )  
                                        
                                    (
                                    (recognise_activity)
                                    ) 
                                    
                                    )
                                                        """
       

    filename = filepaths.path_problemfile_officeroom
    with open(filename, "w") as f:
                f.truncate()
                f.write(output)

    
def clear_input_sensordata_file():
    input_file=open(filepaths.path_sensoroutputsfile_officeroom,"w")
    input_file.truncate()

    
def clear_output_plan_file():
    output_file=open(filepaths.path_planoutputfile_officeroom,"w")
    output_file.truncate()
    


def runActivity_recognition_example():
    os.chdir(filepaths.path_jshop2_directory)
    os.system("{0}{1}{2}{3}{4}{5}{6}".format(filepaths.universalmakestatement,filepaths.universalwhitespace,filepaths.makenumberforofficeroom,filepaths.universalwhitespace,filepaths.universalforwardsymbol,filepaths.universalwhitespace,filepaths.path_planoutputfile_officeroom))
    
    

    
    
    
