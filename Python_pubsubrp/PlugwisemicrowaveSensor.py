from random import random
class Plugwisemicrowave:
    sensorType = "plugwisemicrowave"
    instanceID = "32kd403kwsm"
    unit = "kWh"
    def __init__(self, min, max,avg):
        self. min =  min
        self.max = max
        self.avg=avg
        
        self.value = 0.0
    def sense(self):
    
        self.value = self.complexRandom()
        return self.value
   
    def complexRandom(self):
        value = self.avg * (3 * random() - 1)
        value = max(value, self.min)
        value = min(value, self.max)
        return value 

wmw= Plugwisemicrowave(0.001,1.50005,0.75)
print(wmw.sense())